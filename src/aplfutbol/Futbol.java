package aplfutbol;

import javax.swing.JOptionPane;

/**
 *
 * @author ipujalesvila
 */
public class Futbol {

    private String nom;
    private int dors;

    public Futbol() {
    }

    public Futbol(String nom, int dors) {
        this.nom = nom;
        this.dors = dors;
    }

    public String getNome() {
        return nom;
    }

    public void setNome(String nom) {
        this.nom = nom;
    }

    public int getDorsal() {
        return dors;
    }

    public void setDorsal(int dors) {
        this.dors = dors;
    }

    public static void ver(Futbol[] xog) {
        System.out.println("nom\tdors");
        for (int i = 0; i < xog.length; i++) {
            if (xog[i].nom != null) {
                System.out.println(xog[i].getNome() + "\t" + xog[i].getDorsal());
            }
        }
    }

    public static void buscar(Futbol[] xog, String nom) {
        boolean atopado = false;
        for (int i = 0; i < xog.length; i++) {
            if (xog[i].nom.equalsIgnoreCase(nom)) {
                JOptionPane.showMessageDialog(null, "Nome: " + xog[i].getNome() + "\n"
                        + "Dorsal: " + xog[i].getDorsal());
                atopado = true;
            }
        }
        if (atopado == false) {
            JOptionPane.showMessageDialog(null, "Xogador: " + nom + " non atopado");
        }
    }

    public static void ordenarPorNombre(Futbol[] xog) {
        Futbol aux;
        for (int i = 0; i < xog.length - 1; i++) {
            for (int j = i; j < xog.length; j++) {
                if (xog[i].nom.compareToIgnoreCase(xog[j].nom) > 0) {
                    aux = xog[i];
                    xog[i] = xog[j];
                    xog[j] = aux;
                }
            }
        }
    }

    public static void darBaja(Futbol[] xog, String nom) {
        int cont = 0, cont2 = 0;
        for (int i = 0; i < xog.length; i++) {
            if (xog[i].nom.equalsIgnoreCase(nom)) {
                //Futbol aux;
                xog[i].nom = null;
                xog[i].dors = 0;
                cont = cont + 1;
                for (int h = i; h < xog.length - 1; h++) {
                    xog[h].dors = xog[h + 1].dors;
                    xog[h].nom = xog[h + 1].nom;
                    i++;
                }

            }
        }

        for (int i = xog.length - 1; i > 0; i--) {
            xog[i].nom = null;
            xog[i].dors = 0;
            cont2++;
            if (cont2 == cont) {
                break;
            }

        }

    }
    //Borrado final

    public static void darBaja2(Futbol[] xog, String nom) {
        int cont = 0, cont2 = 0;
        for (int f = 0; f < xog.length; f++) {
            if (xog[f].nom.equalsIgnoreCase(nom)) {
                for (int i = 0; i < xog.length; i++) {
                    if (xog[i].nom.equalsIgnoreCase(nom)) {
                        xog[i].nom = null;
                        xog[i].dors = 0;
                        cont++;
                    }
                }

                for (int i = 0; i < xog.length; i++) {
                    if (xog[i].nom == null) {
                        for (int h = i; h < xog.length; h++) {
                            if (xog[h].nom != null) {
                                xog[i].nom = xog[h].nom;
                                xog[i].dors = xog[h].dors;
                                xog[h].nom = null;
                                xog[h].dors = 0;
                                i++;
                            }
                        }
                    }
                }
                for (int i = xog.length - 1; i > 0; i--) {
                    xog[i].nom = null;
                    xog[i].dors = 0;
                    cont2++;
                    if (cont2 == cont) {
                        break;
                    }

                }

            } else {
                System.out.println("Jugador: " + nom + " no encontrado");
            }


        }

    }
}
